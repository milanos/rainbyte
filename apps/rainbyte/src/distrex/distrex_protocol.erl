%% Feel free to use, reuse and abuse the code in this file.

-module(distrex_protocol).
-behaviour(gen_statem).
-behaviour(ranch_protocol).

%% API.
-export([start_link/4]).

%% gen_statem.
-export([callback_mode/0]).
-export([init/1]).
-export([connected/3]).
-export([terminate/3]).
-export([code_change/4]).

-define(TIMEOUT, 5000).

-record(state, {socket, transport}).

%% API.

start_link(Ref, _Socket, Transport, Opts) ->
	{ok, proc_lib:spawn_link(?MODULE, init, [{Ref, Transport, Opts}])}.

%% gen_statem.

callback_mode() ->
	state_functions.

init({Ref, Transport, _Opts = []}) ->
	{ok, Socket} = ranch:handshake(Ref),
	ok = Transport:setopts(Socket, [{active, once}, binary, {packet, 4}]),
	gen_statem:enter_loop(?MODULE, [], connected,
												#state{socket=Socket, transport=Transport},
												[?TIMEOUT]).

connected(info, {tcp, Socket, Data}, _StateData=#state{
																									 socket=Socket, transport=Transport})
	when byte_size(Data) > 1 ->
	Transport:setopts(Socket, [{active, once}]),
	distribute_reqs(Data, _StateData),
	% Transport:send(Socket, distribute_reqs(Data)),
	{keep_state_and_data, ?TIMEOUT};
connected(info, {tcp_closed, _Socket}, _StateData) ->
	{stop, normal};
connected(info, {tcp_error, _, Reason}, _StateData) ->
	{stop, Reason};
connected({call, From}, _Request, _StateData) ->
	gen_statem:reply(From, ok),
	keep_state_and_data;
connected(cast, _Msg, _StateData) ->
	keep_state_and_data;
connected(timeout, _Msg, _StateData) ->
	{stop, normal};
connected(_EventType, _Msg, _StateData) ->
	{stop, normal}.

terminate(Reason, StateName, StateData=#state{
																					socket=Socket, transport=Transport})
	when Socket=/=undefined andalso Transport=/=undefined ->
	catch Transport:close(Socket),
	terminate(Reason, StateName,
						StateData#state{socket=undefined, transport=undefined});
terminate(_Reason, _StateName, _StateData) ->
	ok.

code_change(_OldVsn, StateName, StateData, _Extra) ->
	{ok, StateName, StateData}.

%% Internal.

distribute_reqs(RawData, _StateData=#state{
																			 socket=Socket, transport=Transport})
	when is_binary(RawData) ->

	case decode_raw(RawData) of
		error ->
			Transport:close(Socket);
		status_check ->
			Transport:send(Socket, status_msg()),
			Transport:close(Socket);
		work_req ->
			enter_queue(Socket)
	end.

decode_raw(RawData) when is_binary(RawData) ->
	try erlang:binary_to_term(RawData) of
		Response -> Response
	catch
		_ -> error
	end.

status_msg() ->
	{todo, create_this}.

enter_queue(Socket) ->
	%% TODO Setup queue
	{Socket,todo}.

