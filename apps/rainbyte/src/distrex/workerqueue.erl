-module(workerqueue).
-behaviour(gen_statem).

-export([start/0, enter_queue/1, start_task/0, status/0, stop/0]).
-export([terminate/3, code_change/4, init/1, callback_mode/0]).

name() -> workerqueue. % The registered server name

%% API.  This example uses a registered name name()
%% and does not link to the caller.
start() ->
  gen_statem:start({local, name()},  ?MODULE,  [],  []).
enter_queue(Socket) ->
  gen_statem:call(name(),  {enter_queue, Socket}).
start_task() ->
  gen_statem:cast(name(), start_task).
status() ->
  gen_statem:call(name(),  status).
stop() ->
  gen_statem:stop(name()).

%% Mandatory callback functions
terminate(_Reason,  _State,  _Queue) ->
  void.
code_change(_Vsn,  State,  Queue,  _Extra) ->
  {ok, State, Queue}.
init([]) ->
  %% Set the initial state + data.  Queue is used only as a counter.
  State = busy,  Queue = queue:new(),
  {ok, State, Queue}.
callback_mode() -> handle_event_function.

%%% state callback(s)

%% Enter socket in queue
handle_event({call,From}, {enter_queue, Socket}, State, Queue) ->
    {next_state,State,queue:in(Socket,Queue),[{reply,From,ok}]};

%% Start Task
handle_event({call,From}, start_task, busy, Queue) ->
    {next_state,busy,Queue,[{reply,From,{error, busy}}]};

handle_event({call,From}, start_task, available, Queue) ->
  %% TODO: figure out how to implement the task
    Responce = {},
    {next_state,busy,Queue,[{reply,From,Responce}]};

%% Reply status with the current queue
handle_event({call,From}, status, State, Queue) ->
    %% TODO: change it to a more featureful status msg
    {next_state,State,Queue,[{reply,From,Queue}]};

handle_event(_, _, State, Queue) ->
    %% Ignore all other events
    {next_state,State,Queue}.
