%% Feel free to use, reuse and abuse the code in this file.

%% @private
-module(distrex_app).
-behaviour(application).

%% API.
-export([start/2]).
-export([stop/1]).

%% Constants.
-define(PORT, 5555).

%% API.

start(_Type, _Args) ->
	{ok, _} = ranch:start_listener(distrex,
		ranch_tcp, [{port, ?PORT}], distrex_protocol, []),
	distrex_sup:start_link().

stop(_State) ->
	ok.
